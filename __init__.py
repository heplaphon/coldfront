from st3m.application import Application, ApplicationContext
import st3m.run
import network
import urequests
import json
import time


class ColdFront(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)
        self._station = network.WLAN(network.STA_IF)
        self._elapsed = 0
        self._data = None
        self._error = None
        try:
            if self._station.isconnected() == True:
                print("Already connected")
            self._station.active(True)
            self._station.connect("Camp2023-open", "")
        except OSError:
            self._error = "WIFI error"

    def get_temp(self) -> None:
        print("Getting temp")
        try:
            response = urequests.get(url='http://151.216.205.243:8080')
            self._data = response.text.split(',')
            self._error = None
        except OSError:
            self._error = "EHOSTUNREACH"


    def draw(self, ctx: Context) -> None:
        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font_size = 45.0
        ctx.font = ctx.get_font_name(1)
        # Paint the background black
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(255, 255, 255)

        # Paint a red square in the middle of the display
        #ctx.rgb(255, 0, 0).rectangle(-20, -20, 40, 40).fill()
        # ctx.image("/flash/sys/troll.png", 0, 0, 100, 100)
        ctx.move_to(0, 0)
        if self._error != None:
            ctx.save()
            ctx.font_size = 20.0
            ctx.scale(1, 1)
            ctx.text(self._error)
            ctx.restore()
        elif self._station.isconnected() == False:
            ctx.save()
            ctx.font_size = 35.0
            ctx.scale(1, 1)
            ctx.text("Connecting")
            ctx.restore()
        elif self._data == None:
            ctx.save()
            ctx.font_size = 35.0
            ctx.scale(1, 1)
            ctx.text("Getting temp")
            ctx.restore()
        else:
            ctx.save()
            ctx.scale(1, 1)
            ctx.text(str(int(self._data[1])/100) + " °C")
            ctx.restore()

    def on_exit(self) -> None:
        if self._station.active() == True:
          self._station.active(False)

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)
        self._elapsed += delta_ms
        if ((self._elapsed%100)>90 or (self._elapsed%100)<10):
            if self._station.isconnected():
                self.get_temp()
        pass


#st3m.run.run_responder(ColdFront())
